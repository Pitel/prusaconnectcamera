plugins {
    id("com.android.application")
    kotlin("android")
    // id("com.google.devtools.ksp")
    // id("com.google.gms.google-services")
    // id("com.google.firebase.crashlytics")
    // id("com.google.firebase.firebase-perf")
    // id("androidx.navigation.safeargs.kotlin")
}

android {
    compileSdk = 34
    namespace = "org.kalabovi.prusaconnectcamera"

    defaultConfig {
        applicationId = "org.kalabovi.prusaconnectcamera"
        minSdk = 21
        targetSdk = 34
        versionCode = 1
        versionName = "0.1.0"
    }

    buildFeatures {
        buildConfig = true
        viewBinding = true
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = "11"
        freeCompilerArgs += "-opt-in=kotlin.RequiresOptIn"
    }
    testOptions.unitTests.all { it.useJUnitPlatform() }
    packagingOptions.resources.excludes += "DebugProbesKt.bin"
    androidResources.generateLocaleConfig = true
}

/*
ksp {
    arg("room.schemaLocation", "$projectDir/schemas")
}
*/

kotlin {
    jvmToolchain(21)
}

dependencies {
    coreLibraryDesugaring("com.android.tools:desugar_jdk_libs_nio:2.0.4")

    val coroutinesVersion = "1.8.0-RC"
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-guava:$coroutinesVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-play-services:$coroutinesVersion")

    implementation("androidx.core:core-ktx:1.13.0-alpha02")
    implementation("androidx.appcompat:appcompat:1.7.0-alpha03")
    // implementation("androidx.fragment:fragment-ktx:1.7.0-alpha03")
    implementation("androidx.activity:activity-ktx:1.9.0-alpha01")
    implementation("androidx.datastore:datastore-preferences:1.1.0-alpha07")
    val lifecycleVersion = "2.7.0-rc02"
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion")
    implementation("androidx.lifecycle:lifecycle-service:$lifecycleVersion")
    // implementation("androidx.recyclerview:recyclerview:1.3.1")

    implementation("com.google.android.material:material:1.12.0-alpha02")
    implementation("androidx.constraintlayout:constraintlayout:2.2.0-alpha13")

    /*
    implementation("com.google.android.play:app-update-ktx:2.1.0")
    implementation("com.google.android.play:review-ktx:2.0.1")
     */

    implementation("com.jakewharton.timber:timber:5.0.1")

    val leakcanaryVersion = "2.12"
    debugImplementation("com.squareup.leakcanary:leakcanary-android:$leakcanaryVersion")
    implementation("com.squareup.leakcanary:plumber-android:$leakcanaryVersion")

    implementation("io.insert-koin:koin-android:3.5.0")

    val okhttpVersion = "4.12.0"
    implementation("com.squareup.okhttp3:okhttp:$okhttpVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:$okhttpVersion")
    val retrofitVersion = "2.9.0"
    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-gson:$retrofitVersion")
    implementation("com.google.code.gson:gson:2.10.1")

    val cameraxVersion = "1.4.0-alpha03"
    implementation("androidx.camera:camera-camera2:$cameraxVersion")
    implementation("androidx.camera:camera-view:$cameraxVersion")
    implementation("androidx.camera:camera-lifecycle:$cameraxVersion")
    implementation("androidx.camera:camera-extensions:$cameraxVersion")

    implementation("com.google.android.gms:play-services-code-scanner:16.1.0")

    /*
    val roomVersion = "2.6.0-beta01"
    implementation("androidx.room:room-ktx:$roomVersion")
    ksp("androidx.room:room-compiler:$roomVersion")
     */

    /*
    implementation(platform("com.google.firebase:firebase-bom:32.2.3"))
    implementation("com.google.firebase:firebase-crashlytics-ktx")
    implementation("com.google.firebase:firebase-perf-ktx")
     */

    /*
    val navVersion = "2.7.1"
    implementation("androidx.navigation:navigation-fragment-ktx:$navVersion")
    implementation("androidx.navigation:navigation-ui-ktx:$navVersion")
     */

    /*
    val kotestVersion = "5.6.2"
    testImplementation("io.kotest:kotest-runner-junit5:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
    val mockkVersion = "1.13.7"
    testImplementation("io.mockk:mockk-android:$mockkVersion")
    testImplementation("io.mockk:mockk-agent:$mockkVersion")
     */
}
