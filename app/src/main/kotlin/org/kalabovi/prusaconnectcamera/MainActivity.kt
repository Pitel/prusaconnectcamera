package org.kalabovi.prusaconnectcamera

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.guava.await
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import org.kalabovi.prusaconnectcamera.api.Repository
import org.kalabovi.prusaconnectcamera.databinding.MainBinding
import org.koin.android.ext.android.inject
import org.koin.androidx.scope.ScopeActivity
import timber.log.Timber
import kotlin.properties.Delegates

class MainActivity : ScopeActivity() {
    private val repository by inject<Repository>()

    private val binding by lazy { MainBinding.inflate(layoutInflater) }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            Timber.d("Permissions $permissions")
            if (permissions.values.all { it }) {
                bindService(Intent(this, CameraService::class.java), serviceConnection, Context.BIND_AUTO_CREATE)
            } else {
                finish()
            }
        }

    private val cameraManager = MutableStateFlow<CameraManager?>(null)

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            cameraManager.value = (service as CameraService.CameraBinder).camera
        }

        override fun onServiceDisconnected(name: ComponentName) {
            cameraManager.value = null
        }
    }

    private var previewJob: Job? by Delegates.observable(null) { _, old, _ ->
        old?.cancel()
    }

    private val serviceIntent by lazy { Intent(this@MainActivity, CameraService::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            try {
                val qrClient = GmsBarcodeScanning.getClient(
                    this@MainActivity,
                    GmsBarcodeScannerOptions.Builder()
                        .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                        .enableAutoZoom()
                        .build()
                )
                while (repository.getToken() == null) {
                    val qr = qrClient.startScan().await().rawValue
                    Timber.d(qr)
                    Toast.makeText(this@MainActivity, "$qr", Toast.LENGTH_SHORT).show()
                    qr?.let { qr ->
                        val groups = TOKEN_REGEX.matchEntire(qr)?.groupValues
                        if (groups?.size == 2) {
                            repository.saveToken(groups.last())
                        }
                    }
                }
                repository.info()
                requestPermissionLauncher.launch(
                    buildList {
                        add(Manifest.permission.CAMERA)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                            add(Manifest.permission.POST_NOTIFICATIONS)
                        }
                    }.toTypedArray()
                )
            } catch (ce: CancellationException) {
                throw ce
            } catch (e: Exception) {
                Timber.w(e)
            }
        }
        setContentView(binding.root)
        repository.error.filterNotNull().onEach {
            Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
            repository.error.value = null
        }.launchIn(lifecycleScope)
        cameraManager.onEach {
            binding.stream.isEnabled = it != null
        }
            .filterNotNull()
            .onEach {
                it.capturing.onEach {capturing ->
                    with(binding.stream) {
                        setOnCheckedChangeListener(null)
                        isChecked = capturing
                        setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                ContextCompat.startForegroundService(
                                    this@MainActivity,
                                    serviceIntent.setAction(CameraService.ACTION_START)
                                )
                            } else {
                                startService(serviceIntent.setAction(CameraService.ACTION_STOP))
                                cameraManager.value?.stopCapturing()
                            }
                        }
                    }
                }.launchIn(lifecycleScope)
            }
            .launchIn(lifecycleScope)
    }

    override fun onStart() {
        super.onStart()
        previewJob = lifecycleScope.launch {
            cameraManager.filterNotNull().first().startPreview(binding.camera.surfaceProvider)
        }
    }

    override fun onStop() {
        previewJob = lifecycleScope.launch {
            cameraManager.filterNotNull().first().stopPreview()
        }
        super.onStop()
    }

    override fun onDestroy() {
        unbindService(serviceConnection)
        super.onDestroy()
    }

    private companion object {
        private val TOKEN_REGEX = """^https://webcam.connect.prusa3d.com\?token=(\w{20})$""".toRegex()
    }
}
