package com.twentyfouri.phenix

import org.koin.core.logger.KOIN_TAG
import org.koin.core.logger.Level
import org.koin.core.logger.Logger
import org.koin.core.logger.MESSAGE
import timber.log.Timber

class KoinTimberLogger(level: Level = Level.INFO) : Logger(level) {
    override fun display(level: Level, msg: MESSAGE) {
        with(Timber.tag(KOIN_TAG)) {
            when (level) {
                Level.DEBUG -> d(msg)
                Level.INFO -> i(msg)
                Level.WARNING -> w(msg)
                Level.ERROR -> e(msg)
                else -> e(msg)
            }
        }
    }
}
