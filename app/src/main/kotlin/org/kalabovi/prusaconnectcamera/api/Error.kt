package org.kalabovi.prusaconnectcamera.api

data class Error(
    val message: String,
    val code: String? = null
)
