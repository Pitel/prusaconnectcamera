package org.kalabovi.prusaconnectcamera.api

import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

data class InfoRequest(
    val config: Config
) {
    data class Config(
        val name: String,
        val trigger_scheme: TriggerScheme = TriggerScheme.THIRTY_SEC
    ) {
        enum class TriggerScheme(val duration: Duration) {
            TEN_SEC(10.seconds),
            THIRTY_SEC(30.seconds),
            SIXTY_SEC(60.seconds),
            TEN_MIN(10.minutes)
        }
    }
}
