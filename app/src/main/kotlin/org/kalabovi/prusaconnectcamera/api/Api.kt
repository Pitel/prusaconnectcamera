package org.kalabovi.prusaconnectcamera.api

import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.PUT

interface Api {
    @PUT("info")
    suspend fun info(
        @Header("Token") token: String,
        @Header("Fingerprint") fingerprint: String,
        @Body body: InfoRequest
    )

    @PUT("snapshot")
    suspend fun snapshot(
        @Header("Token") token: String,
        @Header("Fingerprint") fingerprint: String,
        @Body image: RequestBody
    )
}
