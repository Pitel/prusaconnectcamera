package org.kalabovi.prusaconnectcamera.api

import android.content.Context
import android.os.Build
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import timber.log.Timber

class Repository(
    private val api: Api,
    private val gson: Gson,
    private val context: Context
) {
    val error = MutableStateFlow<Error?>(null)

    suspend fun info() = catchRequest {
        api.info(
            getToken() ?: "",
            FINGERPRINT,
            InfoRequest(InfoRequest.Config(Build.MODEL, InfoRequest.Config.TriggerScheme.TEN_SEC))
        )
    }

    suspend fun snapshot(image: ByteArray) = catchRequest {
        api.snapshot(
            getToken() ?: "",
            FINGERPRINT,
            image.toRequestBody(JPEG)
        )
    }

    private suspend inline fun <T> catchRequest(crossinline request: suspend () -> T): T? = try {
        withContext(Dispatchers.Default) { request() }
    } catch (he: HttpException) {
        Timber.w(he)
        error.value = try {
            gson.fromJson(he.response()?.errorBody()?.string(), Error::class.java)
        } catch (jse: JsonSyntaxException) {
            Timber.w(jse)
            Error(he.message())
        }
        null
    } catch (npe: NullPointerException) {
        Timber.w(npe)
        null
    }

    suspend fun getToken() = context.dataStore.data.first()[TOKEN_KEY]

    suspend fun saveToken(token: String) {
        context.dataStore.edit {
            it[TOKEN_KEY] = token
        }
    }

    private companion object {
        private val JPEG = "image/jpg".toMediaType()
        private val FINGERPRINT = Build.FINGERPRINT.padEnd(16, '_').take(64)
        private val Context.dataStore by preferencesDataStore("prefs")
        private val TOKEN_KEY = stringPreferencesKey("token")
    }
}
