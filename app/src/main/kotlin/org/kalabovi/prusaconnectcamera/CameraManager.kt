package org.kalabovi.prusaconnectcamera

import android.content.Context
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.core.Preview.SurfaceProvider
import androidx.camera.core.UseCaseGroup
import androidx.camera.core.resolutionselector.ResolutionSelector
import androidx.camera.extensions.ExtensionMode
import androidx.camera.extensions.ExtensionsManager
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.runningFold
import kotlinx.coroutines.guava.await
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.kalabovi.prusaconnectcamera.api.InfoRequest
import org.kalabovi.prusaconnectcamera.api.Repository
import timber.log.Timber
import java.io.ByteArrayOutputStream

class CameraManager(
    private val context: Context,
    private val lifecycle: LifecycleOwner,
    private val repository: Repository
) {
    private val cameraProvider = ProcessCameraProvider.getInstance(context.applicationContext)
    private val cameraSelector = lifecycle.lifecycleScope.async {
        try {
            ExtensionsManager.getInstanceAsync(context.applicationContext, cameraProvider.await())
                .await()
                .getExtensionEnabledCameraSelector(CAMERA_SELECTOR, ExtensionMode.AUTO)
        } catch (iae: IllegalArgumentException) {
            Timber.w(iae)
            CAMERA_SELECTOR
        }
    }

    private val preview = MutableStateFlow<Preview?>(null)
    private val capture = MutableStateFlow<ImageCapture?>(null).apply {
        runningFold<ImageCapture?, Job?>(null) { prevJob, capture ->
            prevJob?.cancel()
            if (capture != null) {
                lifecycle.lifecycleScope.launch {
                    while (isActive) {
                        val outStream = ByteArrayOutputStream()
                        capture.takePicture(
                            ImageCapture.OutputFileOptions.Builder(outStream).build(),
                            ContextCompat.getMainExecutor(context.applicationContext),
                            object : ImageCapture.OnImageSavedCallback {
                                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                                    val data = outStream.use { it.toByteArray() }
                                    lifecycle.lifecycleScope.launch(Dispatchers.Default) {
                                        repository.snapshot(data)
                                    }
                                }

                                override fun onError(exception: ImageCaptureException) {
                                    Timber.w(exception)
                                    outStream.close()
                                }
                            }
                        )
                        delay(InfoRequest.Config.TriggerScheme.TEN_SEC.duration)
                    }
                }
            } else {
                null
            }
        }.launchIn(lifecycle.lifecycleScope)
    }

    val previewing = preview.map { it != null }
    val capturing = capture.map { it != null }

    private val useCases = preview.combine(capture) { preview, capture ->
        if (preview != null || capture != null) {
            UseCaseGroup.Builder().apply {
                preview?.let {
                    addUseCase(it)
                    Timber.d("Added preview")
                }
                capture?.let {
                    addUseCase(it)
                    Timber.d("Added capture")
                }
            }.build()
        } else {
            null
        }
    }

    init {
        require(context == context.applicationContext)
        useCases.onEach {
            cameraProvider.await().apply {
                unbindAll()
                it?.let {
                    bindToLifecycle(
                        lifecycle,
                        cameraSelector.await(),
                        it
                    )
                }
            }
        }.launchIn(lifecycle.lifecycleScope)
    }

    fun startPreview(surface: SurfaceProvider) {
        Timber.d("startPreview")
        preview.value = PREVIEW.apply { setSurfaceProvider(surface) }
    }

    fun stopPreview() {
        Timber.d("stopPreview")
        preview.value = null
    }

    fun startCapturing() {
        capture.value = IMAGE_CAPTURE
    }

    fun stopCapturing() {
        capture.value = null
    }

    private companion object {
        private const val MAX_HEIGHT = 1080
        private val PREVIEW = Preview.Builder().build()
        private val RESOLUTION_SELECTOR = ResolutionSelector.Builder()
            .setAllowedResolutionMode(
                ResolutionSelector.PREFER_HIGHER_RESOLUTION_OVER_CAPTURE_RATE
            )
            .setResolutionFilter { supportedSizes, _ ->
                supportedSizes.forEach {
                    Timber.v("$it")
                }
                supportedSizes.filter { it.height <= MAX_HEIGHT }
            }
            .build()
        private val IMAGE_CAPTURE = ImageCapture.Builder()
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
            .setJpegQuality(50)
            //.setResolutionSelector(RESOLUTION_SELECTOR)
            .build()
        private val CAMERA_SELECTOR = CameraSelector.DEFAULT_BACK_CAMERA
    }
}
