package org.kalabovi.prusaconnectcamera

import android.app.Application
import android.net.TrafficStats
import android.os.StrictMode
import androidx.fragment.app.FragmentManager
import com.google.android.material.color.DynamicColors
import com.google.gson.Gson
import com.twentyfouri.phenix.KoinTimberLogger
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kalabovi.prusaconnectcamera.api.Api
import org.kalabovi.prusaconnectcamera.api.Repository
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

class App : Application() {
    init {
        TrafficStats.setThreadStatsTag(THREAD_STATS_TAG)
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
            FragmentManager.enableDebugLogging(true)
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)

        startKoin {
            androidContext(this@App)
            if (BuildConfig.DEBUG) {
                logger(KoinTimberLogger(Level.DEBUG))
            }
            modules(
                module {
                    singleOf(::Gson)
                    single {
                        OkHttpClient.Builder()
                            .connectionSpecs(listOf(ConnectionSpec.RESTRICTED_TLS))
                            .apply {
                                if (BuildConfig.DEBUG) {
                                    addNetworkInterceptor(
                                        HttpLoggingInterceptor { message -> Timber.tag("OkHttpClient").v(message) }
                                            .setLevel(HttpLoggingInterceptor.Level.BODY)
                                    )
                                }
                            }
                            .build()
                    }
                    single {
                        Retrofit.Builder()
                            .client(get())
                            .addConverterFactory(GsonConverterFactory.create(get()))
                            .baseUrl("https://webcam.connect.prusa3d.com/c/")
                            .build()
                            .create(Api::class.java)
                    }

                    single {
                        CameraManager(androidContext(), it.get(), get())
                    }

                    singleOf(::Repository)
                }
            )
        }
    }

    private companion object {
        private const val THREAD_STATS_TAG = 42
    }
}
