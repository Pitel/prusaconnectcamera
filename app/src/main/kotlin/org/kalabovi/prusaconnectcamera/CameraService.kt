package org.kalabovi.prusaconnectcamera

import android.app.PendingIntent
import android.content.Intent
import android.os.Binder
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.ServiceCompat
import androidx.core.app.TaskStackBuilder
import androidx.lifecycle.LifecycleService
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class CameraService : LifecycleService() {
    override fun onCreate() {
        super.onCreate()
        Timber.d("Created")
    }

    private val camera: CameraManager by inject { parametersOf(this) }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION_START -> {
                val notificationManager = NotificationManagerCompat.from(this)
                notificationManager.createNotificationChannel(
                    NotificationChannelCompat.Builder(
                        NOTIFICATION_CHANNEL_ID,
                        NotificationManagerCompat.IMPORTANCE_LOW
                    ).setName(getString(R.string.service)).build()
                )
                val notification = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.outline_linked_camera_24)
                    .setContentTitle(getString(R.string.app_name))
                    .setOngoing(true)
                    .setContentIntent(
                        TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(Intent(this, MainActivity::class.java))
                            .getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                            )
                    )
                    .build()

                startForeground(NOTIFICATION_ID, notification)
                camera.startCapturing()
            }
            ACTION_STOP -> {
                camera.stopCapturing()
                ServiceCompat.stopForeground(this, ServiceCompat.STOP_FOREGROUND_REMOVE)
                stopSelf()
            }
            else -> Timber.w("Action ${intent?.action}")
        }
        Timber.d("Started")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        Timber.d("Destroyed")
        super.onDestroy()
    }

    override fun onBind(intent: Intent): Binder {
        super.onBind(intent)
        return CameraBinder()
    }

    inner class CameraBinder : Binder() {
        val camera = this@CameraService.camera
    }

    companion object {
        const val ACTION_START = "start"
        const val ACTION_STOP = "stop"
        private const val NOTIFICATION_ID = 84
        private const val NOTIFICATION_CHANNEL_ID = "CaptureService"
    }
}
