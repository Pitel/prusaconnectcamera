// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    val kotlin = "1.9.21"
    id("com.android.application") version "8.3.0-alpha18" apply false
    //id("com.android.library") version "8.3.0-alpha04" apply false
    kotlin("android") version kotlin apply false
    //id("com.google.devtools.ksp") version "$kotlin-1.0.13" apply false
    //id("com.google.gms.google-services") version "4.3.15" apply false
    //id("com.google.firebase.crashlytics") version "2.9.9" apply false
    //id("com.google.firebase.firebase-perf") version "1.4.2" apply false
    //id("androidx.navigation.safeargs.kotlin") version "2.7.1" apply false
}
